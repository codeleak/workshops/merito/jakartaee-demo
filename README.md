# Jakarta EE Demo
> Generated with https://start.jakartaee.io


## Pre-requisites

- Java 17

## Running the application

> Note, the [Maven Wrapper](https://maven.apache.org/wrapper/) is already included in the project, so a Maven install is not actually needed. 
> You may first need to execute `chmod +x mvnw`.

```
./mvnw clean package tomee:run
```

Once the runtime starts, you can access the project at http://localhost:8080/jakartaee-demo
